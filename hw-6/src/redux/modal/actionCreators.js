import { OPEN_MODAL, CLOSE_MODAL } from "./actions";

export const openModal = (modalType, onClickConfirm, onClickCancel, onClose) => ({ type: OPEN_MODAL, payload: { modalType, onClickConfirm, onClickCancel, onClose }});
export const closeModal = () => ({ type: CLOSE_MODAL });