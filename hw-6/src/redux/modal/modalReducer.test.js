import modalReducer, { initialValue } from './reducer';
import { openModal, closeModal } from './actionCreators';

describe ('Modal reducer test', () => {
    test('should modalReducer return default value without state and action', () => {
        expect(modalReducer()).toBe(initialValue)
    })

    test('should modalReducer OPEN_MODAL change isOpen value to true', () => {
        const onClickConfirmMock = jest.fn();
        const onClickCancelMock = jest.fn();
        const onCloseMock = jest.fn();

        expect(modalReducer(undefined, openModal('addToCart', onClickConfirmMock, onClickCancelMock, onCloseMock))).toEqual({
            isModalOpen: true,
            modalType: 'addToCart',
            onClickConfirm: onClickConfirmMock,
            onClickCancel: onClickCancelMock,
            onClose: onCloseMock
        })
    })

    test('should modalReducer CLOSE_MODAL change isOpen value to false', () => {
        
        openModal()

        expect(modalReducer(undefined, closeModal())).toEqual(initialValue)
    })

})

