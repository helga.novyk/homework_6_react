import { OPEN_MODAL, CLOSE_MODAL} from "./actions";

export const initialValue = {
    isModalOpen: false,
    modalType: null,
    onClickConfirm: null,
    onClickCancel: null,
    onClose: null
};

const modalReducer = (state = initialValue, action) => {
    switch (action?.type) {
        case OPEN_MODAL: {
            return { ...state, 
                isModalOpen: true, 
                modalType: action.payload.modalType,
                onClickConfirm: action.payload.onClickConfirm, 
                onClickCancel: action.payload.onClickCancel,
                onClose: action.payload.onClose
             }
        }
        case CLOSE_MODAL: {
            return { ...state, 
                isModalOpen: false, 
                modalType: null,
                onClickConfirm: null, 
                onClickCancel: null,
                onClose: null
            }
        }

        default: return state;
    }
}

export default modalReducer;