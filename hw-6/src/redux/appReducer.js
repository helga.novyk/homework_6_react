import { combineReducers } from "redux";
import itemsReducer from "./items/reducer";
import favoritesReducer from "./favorites/reducer";
import cartReducer from "./cart/reducer";
import modalReducer from "./modal/reducer";

const appReducer = combineReducers({
    items: itemsReducer,
    favorites: favoritesReducer,
    cartItems: cartReducer,
    modal: modalReducer
});

export default appReducer;