import { SET_CART, SET_LOADING_CART, SET_ERROR_CART, ADD_TO_CART, DELETE_FROM_CART} from "./actions";

const savedCart = localStorage.getItem("cart");
const initialCartItems = savedCart ? JSON.parse(savedCart) : [];

const initialValue = {
    cartItems: initialCartItems,
    loadingCart: true,
    errorCart: ""
};

const cartReducer = (state = initialValue, action) => {
    switch (action?.type) {
        case SET_CART: {
            return {...state, cartItems: action.payload}
        }
        case SET_LOADING_CART: {
            return {...state, loadingCart: action.payload}
        }
        case SET_ERROR_CART: {
            return {...state, errorCart: action.payload}
        }
        case ADD_TO_CART: {

            const item = action.payload;
            const newCartItems = [...state.cartItems, item];
            return { ...state, cartItems: newCartItems };
        }
        case DELETE_FROM_CART: {
            const itemUniqueId = action.payload.uniqueId;
            const newCartItems = state.cartItems.filter(cartItem => cartItem.uniqueId !== itemUniqueId);
            return { ...state, cartItems: newCartItems }; 
        }

        default: return state;
    }
}

export default cartReducer;