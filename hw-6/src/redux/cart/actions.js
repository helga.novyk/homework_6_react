export const SET_CART = "SET_CART";
export const SET_LOADING_CART = "SET_LOADING_CART";
export const SET_ERROR_CART = "SET_ERROR_CART";
export const ADD_TO_CART = "ADD_TO_CART";
export const DELETE_FROM_CART = "DELETE_FROM_CART";