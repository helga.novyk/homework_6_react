import { SET_CART, SET_LOADING_CART, SET_ERROR_CART, ADD_TO_CART, DELETE_FROM_CART} from "./actions";

export const setCart = (cartItems) => ({ type: SET_CART, payload: cartItems});
export const setLoadingCart = (value) => ({ type: SET_LOADING_CART, payload: value});
export const setErrorCart = (error) => ({ type: SET_ERROR_CART, payload: error});
export const addToCart = (item) => ({ type: ADD_TO_CART, payload: item});
export const deleteFromCart = (item) => ({ type: DELETE_FROM_CART, payload: item});

export const fetchCart = () => {
    return (dispatch) => {
        try {
            const savedCart = localStorage.getItem("cart");
            const cartItems = savedCart ? JSON.parse(savedCart) : [];
            dispatch(setCart(cartItems));
        } catch (error) {
            console.error("ERROR ", error);
            dispatch(setErrorCart(error.message));
        } finally {
            dispatch(setLoadingCart(false));
        }
    }
}

export const postCart = (item) => {
    return (dispatch, getState) => {
        const uniqueCartItem = {
            ...item,
            uniqueId: `${item.id}-${new Date().getTime()}`
        }

        dispatch(addToCart(uniqueCartItem));

        const updatedCart = getState().cartItems.cartItems;
        localStorage.setItem("cart", JSON.stringify(updatedCart));
    }
}

export const fetchDeleteFromCart = (item) => {
    return (dispatch, getState) => {
        dispatch(deleteFromCart(item));

        const currentCart = getState().cartItems.cartItems;
        localStorage.setItem("cart", JSON.stringify(currentCart));
    }
}

