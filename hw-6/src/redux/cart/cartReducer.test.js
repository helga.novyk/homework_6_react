import cartReducer from './reducer';
import { setCart, setLoadingCart, setErrorCart, addToCart, deleteFromCart } from './actionCreators';

const mockLocalStorage = (() => {
    let store = {};
    
    return {
        getItem(key) {
            return store[key] || null;
        },
        setItem(key, value) {
            store[key] = value.toString();
        },
        clear() {
            store = {};
        }
    };
})();

global.localStorage = mockLocalStorage;

describe ('Cart reducer test', () => {
    let initialValue;

    beforeEach(() => {
        localStorage.clear();
        
        const savedCart = localStorage.getItem("cart");
        const initialCartItems = savedCart ? JSON.parse(savedCart) : [];

        initialValue = {
            cartItems: initialCartItems,
            loadingCart: true,
            errorCart: ""
        };
    });

    test('should cartReducer return the default value without state and action', () => {
        expect(cartReducer()).toEqual(initialValue)
    })

    test('should cartReducer SET_CART return items as array of objects for existing cart items', () => {
        const existingCart = [
            { id: 1, name: 'Item 1' }, 
            { id: 2, name: 'Item 2' },
            { id: 3, name: 'Item 3' }
        ];
        localStorage.setItem('cart', JSON.stringify([existingCart]));

        const updatedCart = JSON.parse(localStorage.getItem("cart"));
        const updatedInitialState = {
            ...initialValue,
            cartItems: updatedCart
        };

        expect(cartReducer(undefined, setCart(updatedInitialState.cartItems))).toEqual(updatedInitialState)
    })

    test('should cartReducer SET_CART return empty array for no cart items', () => {
        expect(cartReducer(undefined, setCart([]))).toEqual(initialValue)
    })
    
    test('should cartReducer SET_LOADING_CART change isLoading state', () => {
        expect(cartReducer(undefined, setLoadingCart(false))).toEqual({
            ...initialValue,
            loadingCart: false
        })
    })

    test('should cartReducer SET_ERROR_CART return the error', () => {
        const testError = {
            message: "New error"
        }

        expect(cartReducer(undefined, setErrorCart(testError))).toEqual({
            ...initialValue,
            errorCart: testError
        })
    })

    test('should cartReducer ADD_TO_CART add an item to the cart', () => {
        const existingCart = [
            { id: 1, name: 'Item 1' }, 
            { id: 1, name: 'Item 1' },
            { id: 2, name: 'Item 2' }
        ];
        localStorage.setItem('cart', JSON.stringify([existingCart]));

        const updatedCart = JSON.parse(localStorage.getItem("cart"));
        const updatedInitialState = {
            ...initialValue,
            cartItems: updatedCart
        };
        
        const newCartItem = {id:5, name: "test cart item"};
        
        expect(cartReducer(updatedInitialState, addToCart(newCartItem))).toEqual({
            ...updatedInitialState,
            cartItems: [...updatedInitialState.cartItems, newCartItem]
        })
    })

    test('should cartReducer ADD_TO_CART add to the cart an item with the same id', () => {
        const existingCart = { id: 1, name: "Item 1" };
        localStorage.setItem('cart', JSON.stringify([existingCart]));

        const updatedCart = JSON.parse(localStorage.getItem("cart"));
        const updatedInitialState = {
            ...initialValue,
            cartItems: updatedCart
        };

        const newCartItem = {id: 1, name: "test cart item"};
        
        expect(cartReducer(updatedInitialState, addToCart(newCartItem))).toEqual({
            ...updatedInitialState,
            cartItems: [...updatedInitialState.cartItems, newCartItem]
        })
    })

    test('should cartReducer DELETE_FROM_CART delete an item with uniqueId from cart', () => {
        const existingCart = [
            { id: 1, name: 'Item 1', uniqueId: '1-214728012024' }, 
            { id: 1, name: 'Item 1', uniqueId: '1-214028012024' }, 
            { id: 2, name: 'Item 2', uniqueId: '2-213028012024' }
        ];
        localStorage.setItem('cart', JSON.stringify(existingCart));

        const updatedCart = JSON.parse(localStorage.getItem("cart"));
        const updatedInitialState = {
            ...initialValue,
            cartItems: updatedCart
        };

        const itemForDeleting = { id: 1, name: 'Item 1', uniqueId: '1-214728012024' };
        
        expect(cartReducer(updatedInitialState, deleteFromCart(itemForDeleting))).toEqual({
            ...updatedInitialState,
            cartItems: updatedInitialState.cartItems.filter(item => item.uniqueId !== itemForDeleting.uniqueId)
        })
    })

})