import { SET_ITEM, SET_LOADING, SET_ERROR } from "./actions";

export const setItems = (items) => ({ type: SET_ITEM, payload: items});
export const setLoading = (value) => ({ type: SET_LOADING, payload: value});
export const setError = (error) => ({ type: SET_ERROR, payload: error});

export const fetchItems = () => {
    return async (dispatch) => {
        try {
            const response = await fetch('/manifest.json');
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();

            dispatch(setItems(data.products));
            dispatch(setLoading(false));
        } catch (error) {
            console.error("ERROR", error);
            dispatch(setError(error.message));
        } finally {
            dispatch(setLoading(false));
        }
    }
}