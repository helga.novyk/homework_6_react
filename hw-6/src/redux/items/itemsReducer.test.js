import itemsReducer, { initialValue } from './reducer';
import { setItems, setLoading, setError } from './actionCreators';

describe ('Items reducer test', () => {
    test('should itemsReducer return default value without state and action', () => {
        expect(itemsReducer()).toEqual(initialValue)
    })

    test('should itemsReducer SET_ITEM return items as array of objects', () => {
        const item1 = {};
        const item2 = {};

        expect(itemsReducer(undefined, setItems([item1, item2]))).toEqual({
            ...initialValue,
            items: [item1, item2]
        })
    })

    test('should itemsReducer SET_LOADING change isLoading state', () => {
        expect(itemsReducer(undefined, setLoading(false))).toEqual({
            ...initialValue,
            loading: false,
        })
    })

    test('should itemsReducer SET_ERROR return the error', () => {
        const testError = {
            message: "New error"
        }

        expect(itemsReducer(undefined, setError(testError))).toEqual({
            ...initialValue,
            error: testError
        })
    })
})