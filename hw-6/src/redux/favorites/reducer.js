import { SET_FAVORITES, SET_LOADING_FAVORITES, SET_ERROR_FAVORITES, ADD_FAVORITE, DELETE_FAVORITE, UPDATE_ITEM_FAVORITE_STATUS } from "./actions";

const savedFavorites = localStorage.getItem("favorites");
const initialFavorites = savedFavorites ? JSON.parse(savedFavorites) : [];

export const initialValue = {
    favorites: initialFavorites,
    loadingFavorites: true,
    errorFavorites: ""
};

const favoritesReducer = (state = initialValue, action) => {
    switch (action?.type) {
        case SET_FAVORITES: {
            return {...state, favorites: action.payload}
        }
        case SET_LOADING_FAVORITES: {
            return {...state, loadingFavorites: action.payload}
        }
        case SET_ERROR_FAVORITES: {
            return {...state, errorFavorites: action.payload}
        }
        case ADD_FAVORITE: {
            const item = action.payload;

            if (state.favorites.some(fav => fav.id === item.id)) {
                return state;
            } else {
                const newFavorites = [...state.favorites, item];
                return { ...state, favorites: newFavorites}
            }
        }
        case DELETE_FAVORITE: {
            const itemId = action.payload.id;
            const newFavorites = state.favorites.filter(fav => fav.id !== itemId);
            return { ...state, favorites: newFavorites }; 
        }
        case UPDATE_ITEM_FAVORITE_STATUS: {
            const updatedFavorites = state.favorites.map(item => 
                item.id === action.payload.id ? { ...item, isFavorite: action.payload.isFavorite } : item);
            return { ...state, favorites: updatedFavorites };
        }


        default: return state;
    }
}

export default favoritesReducer;