import { SET_FAVORITES, SET_LOADING_FAVORITES, SET_ERROR_FAVORITES, ADD_FAVORITE, DELETE_FAVORITE, UPDATE_ITEM_FAVORITE_STATUS } from "./actions";

export const setFavorites = (favorites) => ({ type: SET_FAVORITES, payload: favorites});
export const setLoadingFavorites = (value) => ({ type: SET_LOADING_FAVORITES, payload: value});
export const setErrorFavorites = (error) => ({ type: SET_ERROR_FAVORITES, payload: error});
export const addFavorite = (item) => ({ type: ADD_FAVORITE, payload: item});
export const deleteFavorite = (item) => ({ type: DELETE_FAVORITE, payload: item});
export const updateItemFavoriteStatus = (id, isFavorite) => {return {type: UPDATE_ITEM_FAVORITE_STATUS, payload: { id, isFavorite }};}


export const fetchFavorites = () => {
    return (dispatch) => {
        try {
            const savedFavorites = localStorage.getItem("favorites");
            const favorites = savedFavorites ? JSON.parse(savedFavorites) : [];
            dispatch(setFavorites(favorites));
        } catch (error) {
            console.error("ERROR ", error);
            dispatch(setErrorFavorites(error.message));
        } finally {
            dispatch(setLoadingFavorites(false));
        }
    }
}

export const postFavorite = (item) => {
    return (dispatch, getState) => {
        dispatch(addFavorite(item));
        dispatch(updateItemFavoriteStatus(item.id, true));

        const updatedFavorites = getState().favorites.favorites;
        localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    }
}

export const fetchDeleteFavorite = (item) => {
    return (dispatch, getState) => {
        dispatch(deleteFavorite(item));
        dispatch(updateItemFavoriteStatus(item.id, false));

        const currentFavorites = getState().favorites.favorites;
        localStorage.setItem("favorites", JSON.stringify(currentFavorites));
    }
}