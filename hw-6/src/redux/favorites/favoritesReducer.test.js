import favoritesReducer from './reducer';
import { setFavorites, setLoadingFavorites, setErrorFavorites, addFavorite, deleteFavorite, updateItemFavoriteStatus } from './actionCreators';

const mockLocalStorage = (() => {
    let store = {};
    
    return {
        getItem(key) {
            return store[key] || null;
        },
        setItem(key, value) {
            store[key] = value.toString();
        },
        clear() {
            store = {};
        }
    };
})();

global.localStorage = mockLocalStorage;

describe ('Favorites reducer test', () => {
    let initialValue;

    beforeEach(() => {
        localStorage.clear();
        
        const savedFavorites = localStorage.getItem("favorites");
        const initialFavorites = savedFavorites ? JSON.parse(savedFavorites) : [];
        initialValue = {
            favorites: initialFavorites,
            loadingFavorites: true,
            errorFavorites: ""
        };
    });

    test('should favoritesReducer return the default value without state and action', () => {
        expect(favoritesReducer()).toEqual(initialValue)
    })

    test('should favoritesReducer SET_FAVORITES return items as array of objects for existing favorites', () => {
        const existingFavorite = [
            { id: 1, name: 'Item 1' }, 
            { id: 2, name: 'Item 2' }
        ];
        localStorage.setItem('favorites', JSON.stringify(existingFavorite));

        const updatedFavorites = JSON.parse(localStorage.getItem("favorites"));
        const updatedInitialState = {
            ...initialValue,
            favorites: updatedFavorites
        };

        expect(favoritesReducer(undefined, setFavorites(updatedInitialState.favorites))).toEqual(updatedInitialState)
    })

    test('should favoritesReducer SET_FAVORITES return empty array for no favorites', () => {
        expect(favoritesReducer(undefined, setFavorites([]))).toEqual(initialValue)
    })
    
    test('should favoritesReducer SET_LOADING_FAVORITES change isLoading state', () => {
        expect(favoritesReducer(undefined, setLoadingFavorites(false))).toEqual({
            ...initialValue,
            loadingFavorites: false
        })
    })

    test('should favoritesReducer SET_ERROR_FAVORITES return the error', () => {
        const testError = {
            message: "New error"
        }

        expect(favoritesReducer(undefined, setErrorFavorites(testError))).toEqual({
            ...initialValue,
            errorFavorites: testError
        })
    })

    test('should favoritesReducer ADD_FAVORITE add to the favorites an item that was not in favorites', () => {
        const existingFavorite = [
            { id: 1, name: 'Item 1' }, 
            { id: 2, name: 'Item 2' }
        ];
        localStorage.setItem('favorites', JSON.stringify([existingFavorite]));

        const updatedFavorites = JSON.parse(localStorage.getItem("favorites"));
        const updatedInitialState = {
            ...initialValue,
            favorites: updatedFavorites
        };
        
        const newFavorite = {id:5, name: "test favorite"};
        
        expect(favoritesReducer(updatedInitialState, addFavorite(newFavorite))).toEqual({
            ...updatedInitialState,
            favorites: [...updatedInitialState.favorites, newFavorite]
        })
    })

    test('should favoritesReducer ADD_FAVORITE return default value for an item that is in favorites', () => {
        const existingFavorite = { id: 1, name: "Item 1" };
        localStorage.setItem('favorites', JSON.stringify([existingFavorite]));

        const updatedFavorites = JSON.parse(localStorage.getItem("favorites"));
        const updatedInitialState = {
            ...initialValue,
            favorites: updatedFavorites
        };

        const newFavorite = {id: 1, name: "test favorite"};
        
        expect(favoritesReducer(updatedInitialState, addFavorite(newFavorite))).toEqual(updatedInitialState)
    })

    test('should favoritesReducer DELETE_FAVORITE delete from favorites for an item that is in favorites', () => {
        const existingFavorite = [
            { id: 1, name: 'Item 1' }, 
            { id: 2, name: 'Item 2' }
        ];
        localStorage.setItem('favorites', JSON.stringify(existingFavorite));

        const updatedFavorites = JSON.parse(localStorage.getItem("favorites"));
        const updatedInitialState = {
            ...initialValue,
            favorites: updatedFavorites
        };

        const itemForDeleting = {id: 1, name: 'Item 1'};
        
        expect(favoritesReducer(updatedInitialState, deleteFavorite(itemForDeleting))).toEqual({
            ...updatedInitialState,
            favorites: updatedInitialState.favorites.filter(item => item.id !== itemForDeleting.id)
        })
    })

    test('should favoritesReducer UPDATE_ITEM_FAVORITE_STATUS update the isFavorite status of an item', () => {
        const existingFavorites = [
            { id: 1, name: 'Item 1', isFavorite: true },
            { id: 2, name: 'Item 2', isFavorite: true }
        ];
        localStorage.setItem('favorites', JSON.stringify(existingFavorites));
    
        const updatedInitialState = {
            ...initialValue,
            favorites: existingFavorites
        };
    
        const itemIdToUpdate = 1;
        const newFavoriteStatus = false;
        
        const expectedFavorites = [
            { id: 1, name: 'Item 1', isFavorite: newFavoriteStatus }, 
            { id: 2, name: 'Item 2', isFavorite: true }
        ];
    
        expect(favoritesReducer(updatedInitialState, updateItemFavoriteStatus(itemIdToUpdate, newFavoriteStatus))).toEqual({
            ...updatedInitialState,
            favorites: expectedFavorites
        });
    });
})