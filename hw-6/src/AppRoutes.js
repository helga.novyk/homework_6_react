import React from 'react';
import { Routes, Route } from 'react-router-dom';
import Layout from './components/Layout/Layout';
import MainPage from './pages/MainPage';
import FavoritesPage from './pages/FavoritesPage';
import CartPage from './pages/CartPage';

function AppRoutes() {
    return (   
        <Routes>
            <Route path="/" element={<Layout />}>
                <Route index element={<MainPage />} />
                <Route path="cart" element={<CartPage />} />
                <Route path="favorites" element={<FavoritesPage />} />
            </Route>
            <Route path="*" element={<h1>404 - PAGE NOT FOUND</h1>} />
        </Routes>
    );
}

export default AppRoutes;
