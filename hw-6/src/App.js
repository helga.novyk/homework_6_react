import './App.scss';
import React, { useEffect } from 'react';
import { useDispatch} from "react-redux";
import { fetchItems} from './redux/items/actionCreators';
import { fetchFavorites } from './redux/favorites/actionCreators';
import { fetchCart } from "./redux/cart/actionCreators";
import AppRoutes from './AppRoutes';


function App() {

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchItems());
        dispatch(fetchFavorites());
        dispatch(fetchCart())
    }, []);

    return (
        <AppRoutes />
    )
}

export default App;
