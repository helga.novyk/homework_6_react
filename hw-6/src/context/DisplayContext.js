import React, { createContext, useContext, useState } from 'react';

const DisplayContext = createContext();

export const useDisplay = () => useContext(DisplayContext);

export const DisplayProvider = ({ children }) => {
    const [displayMode, setDisplayMode] = useState('cards'); 

    const toggleDisplayMode = () => {
        setDisplayMode(prevMode => prevMode === 'cards' ? 'table' : 'cards');
    };

    return (
        <DisplayContext.Provider value={{ displayMode, toggleDisplayMode }}>
            {children}
        </DisplayContext.Provider>
    );
};
