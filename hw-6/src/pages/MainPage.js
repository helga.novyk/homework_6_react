import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import ItemsContainer from '../components/ItemsContainer/ItemsContainer';
import Modal from "../components/Modal/Modal";
import { useModalContent } from "../hooks/useModalContent";
import { DisplayProvider, useDisplay } from '../context/DisplayContext';

function MainPage () {

    const items = useSelector ((state) => state.items.items, shallowEqual);
    const loading = useSelector((state) => state.items.loading);
    const error = useSelector((state) => state.items.error);

    const { isModalOpen, modalType, onClickConfirm, onClickCancel, onClose } = useSelector(state => state.modal);
    const modalContent = useModalContent(modalType, onClickConfirm, onClickCancel, onClose);

    const ToggleDisplayButton = () => {
        const { toggleDisplayMode, displayMode } = useDisplay();
        return <button onClick={toggleDisplayMode}>{displayMode === 'cards' ? "Таблиця" : "Картки"}</button>;
    };

    return (
        <DisplayProvider>
            <div className="container">
                <div className='display-toggle'>
                    <ToggleDisplayButton />
                </div>
                <ItemsContainer 
                    items={items}
                    loading={loading}
                    error={error}
                    emptyMessage="Товарів немає."
                />

                {isModalOpen && modalContent && (
                    <Modal 
                        header={modalContent.header} 
                        text={modalContent.text} 
                        actions={modalContent.actions}
                        onClose={modalContent.onClose}
                    />
                )}
            </div>
        </DisplayProvider>
    )
}

export default MainPage;