import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import ItemsContainer from '../components/ItemsContainer/ItemsContainer';
import Modal from "../components/Modal/Modal";
import { useModalContent } from "../hooks/useModalContent";
import { DisplayProvider, useDisplay } from '../context/DisplayContext';

const FavoritesPage = () => {

    const favorites = useSelector ((state) => state.favorites.favorites, shallowEqual);
    const loadingFavorites = useSelector((state) => state.favorites.loadingFavorites);
    const errorFavorites = useSelector((state) => state.favorites.errorFavorites);

    const { isModalOpen, modalType, onClickConfirm, onClickCancel, onClose } = useSelector(state => state.modal);
    const modalContent = useModalContent(modalType, onClickConfirm, onClickCancel, onClose);

    const ToggleDisplayButton = () => {
        const { toggleDisplayMode, displayMode } = useDisplay();
        return <button onClick={toggleDisplayMode}>{displayMode === 'cards' ? "Таблиця" : "Картки"}</button>;
    };
    
    return (
        <DisplayProvider>
            <div className="container">
                <div className='display-toggle'>
                    <ToggleDisplayButton />
                </div>

                <ItemsContainer 
                    items={favorites}
                    loading={loadingFavorites}
                    error={errorFavorites}
                    emptyMessage="У вас ще немає вибраних товарів"
                />

                {isModalOpen && modalContent && (
                    <Modal 
                        header={modalContent.header} 
                        text={modalContent.text} 
                        actions={modalContent.actions}
                        onClose={modalContent.onClose}
                    />
                )}
            </div>
        </DisplayProvider>
    )
}

export default FavoritesPage;