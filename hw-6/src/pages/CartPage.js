import React from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import ItemsContainer from '../components/ItemsContainer/ItemsContainer';
import Modal from "../components/Modal/Modal";
import { useModalContent } from "../hooks/useModalContent";
import CheckoutForm from '../components/forms/CheckoutForm/CheckoutForm';
import { DisplayProvider, useDisplay } from '../context/DisplayContext';

function CartPage () {

    const cartItems = useSelector ((state) => state.cartItems.cartItems, shallowEqual);
    const loadingCart = useSelector((state) => state.cartItems.loadingCart);
    const errorCart = useSelector((state) => state.cartItems.errorCart);

    const { isModalOpen, modalType, onClickConfirm, onClickCancel, onClose } = useSelector(state => state.modal);
    const modalContent = useModalContent(modalType, onClickConfirm, onClickCancel, onClose);

    const ToggleDisplayButton = () => {
        const { toggleDisplayMode, displayMode } = useDisplay();
        return <button onClick={toggleDisplayMode}>{displayMode === 'cards' ? "Таблиця" : "Картки"}</button>;
    };

    return (
        <DisplayProvider>
            <div className="container">
                <div className='display-toggle'>
                    <ToggleDisplayButton />
                </div>
                <ItemsContainer 
                    items={cartItems}
                    loading={loadingCart}
                    error={errorCart}
                    emptyMessage="Ваш кошик порожній"
                    removeButton={true}
                />

                {isModalOpen && modalContent && (
                    <Modal 
                        header={modalContent.header} 
                        text={modalContent.text} 
                        actions={modalContent.actions}
                        onClose={modalContent.onClose}
                    />
                )}

                    {cartItems.length > 0 && <CheckoutForm />}
            </div>
        </DisplayProvider>
    )
}

export default CartPage;