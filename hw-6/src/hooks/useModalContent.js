
export function useModalContent(modalType, onClickConfirm, onClickCancel, onClose) {

    switch (modalType) {
        case 'addToCart':
            return {
                header: "Add Product to Cart?",
                text: (
                    <>
                        This product is ready to be added to your cart.
                        <br /><br />
                        Would you like to proceed?"
                    </>
                ),
                actions: [
                    {
                        text: "Confirm",
                        backgroundColor: "#4d1e40",
                        onClick: onClickConfirm
                    },
                    {
                        text: "Cancel",
                        backgroundColor: "#4d1e40",
                        onClick: onClickCancel
                    }
                ],
                onClose: onClose
            };
        case 'removeFromCart':
            return {
                header: "Do you want to delete this product?",
                text: (
                    <>
                        You can later add this product to your cart again.
                        <br /><br />
                        Are you sure you want to delete now?
                    </>
                    ),
                actions: [
                    {
                        text: "Confirm",
                        backgroundColor: "#4d1e40",
                        onClick: onClickConfirm
                    },
                    {
                        text: "Cancel",
                        backgroundColor: "#4d1e40",
                        onClick: onClickCancel
                    }
                ],
                onClose: onClose
            };
        
        default: return null;
    }
}