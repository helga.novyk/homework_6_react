import React from "react";
import { NavLink} from 'react-router-dom';
import styles from './Nav.module.scss';

const Nav = () => {

    return (
        <nav className={styles.header__nav}>
            <NavLink to="/">Головна</NavLink>
            <NavLink to="/favorites">Вибране</NavLink>
            <NavLink to="/cart">Кошик</NavLink>
        </nav>
    )
}

export default Nav;