import React from 'react';
import PropTypes from 'prop-types';
import Item from '../Item/Item';
import ItemPlaceholder from '../ItemPlaceholder/ItemPlaceholder';
import styles from './ItemsContainer.module.scss';
import { useDisplay } from '../../context/DisplayContext';

function ItemsContainer ({ items, loading, error, emptyMessage, removeButton}) {
    const { displayMode } = useDisplay();

    const containerClass = displayMode === 'table' ? 'itemsContainerTable' : 'itemsContainerCards';

    return (
        <div className={styles[containerClass]}>

            {loading && items.length === 0 &&
                Array.from({ length: 8 }, (_, index) => (
                    <ItemPlaceholder key={index} />
                ))
            }

            {!loading && items.length > 0 && items.map((item, index) => (
                <Item 
                    key={`${item.id}-${index}`}
                    id={item.id}
                    name={item.name}
                    price={item.price}
                    imageUrl={item.imageUrl}
                    item={item.item}
                    color={item.color}
                    uniqueId={item.uniqueId}
                    removeButton={removeButton}
                />)
            )}

            {!loading && items.length === 0 && <h3 className={styles.containerClass__emptyMessage}>{emptyMessage}</h3>}

            {error && <h2 className={styles.itemsContainer__error}>{error}</h2>}

        </div>
    )
}

ItemsContainer.propTypes = {
    items: PropTypes.arrayOf(PropTypes.object).isRequired,
    loading: PropTypes.bool.isRequired,
    error: PropTypes.string,
    emptyMessage: PropTypes.string,
    removeButton: PropTypes.bool
}

ItemsContainer.defaultProps = {
    error: "",
    emptyMessage: "",
    removeButton: false
}


export default ItemsContainer;