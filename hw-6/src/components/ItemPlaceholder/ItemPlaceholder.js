import React from "react";
import styles from './ItemPlaceholder.module.scss';

function ItemPlaceholder() {
    
    return (
        <div className={styles.itemPlaceholder}></div>
    )
}

export default ItemPlaceholder;