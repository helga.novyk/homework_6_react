import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Nav from '../Nav/Nav';
import starIcon from '../../constants/star_icon';
import cartIcon from '../../constants/cart_icon';
import homeIcon from '../../constants/home_icon';
import styles from './Header.module.scss';

const Header = () => {
    
    const favoritesCount = useSelector((state) => state.favorites.favorites.length);
    const cartItemsCount = useSelector((state) => state.cartItems.cartItems.length);

    return (
        <header className={styles.header}>
            <div className="container">
                <div className={styles.header__nav__wrapper}>
                    
                    <Link to="/" className={styles.header__homeIcon}>{homeIcon}</Link>
                    
                    <Nav />

                    <div className={styles.header__icon__wrapper}>
                        <Link to="/favorites" className={styles.header__starIcon}>
                            {starIcon}
                            <span className={styles.header__starIcon__badge}>
                                {favoritesCount}
                            </span>
                        </Link>
                        <Link to="/cart" className={styles.header__cartIcon}>
                            {cartIcon}
                            <span className={styles.header__cartIcon__badge}>
                                {cartItemsCount}
                            </span>
                        </Link>
                    </div>
                </div>
            </div>
        </header>
    );
}


export default Header;