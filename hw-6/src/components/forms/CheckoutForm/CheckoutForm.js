import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Formik, Form } from 'formik';
import Input from '../../Input/Input';
import styles from './CheckoutForm.module.scss';
import validationSchema from './validationSchema';
import { fetchDeleteFromCart } from '../../../redux/cart/actionCreators';

const CheckoutForm = () => {

    const initialValues = {
        firstName: "",
        lastName: "",
        age: "",
        deliveryAddress: "",
        cellPhone: ""
    }

    const dispatch = useDispatch();
    const cartItems = useSelector(state => state.cartItems.cartItems);

    const onSubmit = (value, actions) => {

        cartItems.forEach(elem => {
            dispatch(fetchDeleteFromCart(elem));
        });
        console.log("Purchased products: ", cartItems);
        console.log("Customer information: ", value);
        actions.resetForm();
    }


    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
            { (form) => {

                return (
                    <Form className={styles.form}>
                        <p className={styles.title}>Checkout Form</p>
                        <Input id="firstName" name="firstName" label="Ім'я" />
                        <Input id="lastName" name="lastName" label="Прізвище" />
                        <Input id="age" name="age" label="Вік" />
                        <Input id="deliveryAddress" name="deliveryAddress" label="Адреса доставки" />
                        <Input id="cellPhone" name="cellPhone" label="Мобільний телефон" />

                        <button disabled={!(form.isValid && form.dirty)} type="submit">Checkout</button>
                    </Form>
                )
            } }
        </Formik>
    )
}

export default CheckoutForm;