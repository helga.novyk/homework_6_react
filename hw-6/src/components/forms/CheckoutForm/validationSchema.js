import { object, string, number } from 'yup';

const validationSchema = object({
    firstName: string()
        .required("First name is required")
        .trim()
        .matches(/^[A-Za-zА-Яа-яЄєІіЇїҐґ' -]+$/, "First name can only contain Latin or Ukrainian letters, spaces, hyphens, or apostrophes")
        .min(2, "Too short first name"),
    lastName: string()
        .required("Last name is required")
        .trim()
        .matches(/^[A-Za-zА-Яа-яЄєІіЇїҐґ' -]+$/, "Last name can only contain Latin or Ukrainian letters, spaces, hyphens, or apostrophes ")
        .min(2, "Too short last name"),
    age: number()
        .typeError("Age must be a number")
        .min(18, "Min age is 18")
        .max(110, "Max age is 110")
        .integer("Age must be an integer")
        .required("Age is required"),
    deliveryAddress: string()
        .required("Address is required")
        .trim()
        .matches(/.*[A-Za-zА-Яа-яЄєІіЇїҐґ].*/, "Address must contain at least one letter"),
    cellPhone: string()
        .required("Cell phone is required")
        .matches(/^\(\d{3}\) \d{4} \d{3}$/, "Cell phone must match the format (###) #### ###")
})

export default validationSchema;

