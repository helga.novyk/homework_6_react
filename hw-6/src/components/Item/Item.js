import React from "react";
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import StarIcon from "../StarIcon/StarIcon";
import closeIcon from "../../constants/close_icon";
import { useDispatch, useSelector } from "react-redux";
import { postFavorite, fetchDeleteFavorite } from "../../redux/favorites/actionCreators";
import { openModal, closeModal } from "../../redux/modal/actionCreators";
import { postCart, fetchDeleteFromCart } from "../../redux/cart/actionCreators";
import styles from './Item.module.scss';
import { useDisplay } from '../../context/DisplayContext';

function Item ({id, name, price, imageUrl, item, color, removeButton, uniqueId}) {

    const { displayMode } = useDisplay();

    const itemClass = displayMode === 'table' ? 'itemTable' : 'itemCards';

    const dispatch = useDispatch();
    const itemActive = {id, name, price, imageUrl, item, color, uniqueId};
    const isFavorite = useSelector(state => state.favorites.favorites.some(el => el.id === id));
    const isInCart = useSelector(state => state.cartItems.cartItems.some(el => el.id === id));
    
    const handleClickFav = () => {
        if (!isFavorite) {
            dispatch(postFavorite(itemActive));
        } else {
            dispatch(fetchDeleteFavorite(itemActive));
        }
    }

    const addToCart = () => {
           dispatch(postCart(itemActive));
           dispatch(closeModal())
    }

    const removeFromCart = () => {
        dispatch(fetchDeleteFromCart(itemActive));
        dispatch(closeModal())
    }

    const handleAddToCartClick = () => {
        dispatch(openModal('addToCart', addToCart, () => dispatch(closeModal()), () => dispatch(closeModal())));
    }

    const handleRemoveFromCartClick = () => {
        dispatch(openModal('removeFromCart', removeFromCart, () => dispatch(closeModal()), () => dispatch(closeModal())));
    };
       
    return (
        <div className={styles[itemClass]}>
            <div className={styles[`${itemClass}__starIcon__wrapper`]} data-testid="star-icon-wrapper" onClick={handleClickFav}>
                <StarIcon isFavorite={isFavorite} />
            </div>
            
            <div className={styles[`${itemClass}__cover__wrapper`]}>
                <img src={imageUrl} alt={name + " img"}/>
            </div>
            
            <div className={styles[`${itemClass}__content__wrapper`]}>
                <h3>{name}</h3>
                <p>Item: {item}</p>
                <p>Color: {color}</p>
            </div>
            <div className={styles[`${itemClass}__price__wrapper`]}>
                <span>{"$"+ price}</span>
            </div>
            <div className={styles[`${itemClass}__addToCard__wrapper`]}>
                <Button 
                    text={isInCart ? "Added in Cart" : "Add to Cart"}
                    backgroundColor={isInCart ? "#235b33" : "#4d1e40"}
                    onClick={handleAddToCartClick} 
                />
            </div>
            {removeButton && (
                <button className={styles[`${itemClass}__removeCartButton`]} data-testid="removeFromCart-button" onClick={handleRemoveFromCartClick}>{closeIcon}</button>
            )}
        </div>
    )
}

Item.propTypes = {
    id: PropTypes.number.isRequired,
    item: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    imageUrl: PropTypes.string,
    price: PropTypes.number.isRequired,
    color: PropTypes.string,
    uniqueId: PropTypes.string,
    removeButton: PropTypes.bool
}

Item.defaultProps = {
    imageUrl: "https://pienterepiste.be/wp-content/uploads/2023/05/Photo-will-be-soon.jpg",
    color: "original",
    uniqueId: "",
    removeButton: false
}

export default Item;