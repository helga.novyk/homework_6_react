import configureMockStore from 'redux-mock-store';
import { thunk } from 'redux-thunk';
import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import { Provider } from 'react-redux';
import { DisplayProvider } from '../../context/DisplayContext';
import Item from './Item';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const initialState = {
    items: {
        items: [],
        loading: true,
        error: ""
    },
    favorites: {
        favorites: [],
        loadingFavorites: true,
        errorFavorites: ''
    },
    cartItems: {
        cartItems: [],
        loadingCart: true,
        errorCart: ''
    },
};

describe('Tests for Item Component', () => {
    let store;

    beforeEach(() => {
      store = mockStore(initialState);
    });
  
    afterEach(() => {
      store.clearActions();
    });


    test('should render the item (Snapshot) with default props', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        const { asFragment }= render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        expect(screen.getByText('Test Item')).toBeInTheDocument();
        expect(screen.getByText('$100')).toBeInTheDocument();
        expect(screen.getByAltText('Test Item img')).toBeInTheDocument();

        const removeFromCartButton = screen.queryByTestId('removeFromCart-button');
        expect(removeFromCartButton).toBeNull();

        expect(asFragment()).toMatchSnapshot();
    });

    test('should render the item (Snapshot) when removeButton is true', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id',
            removeButton: true
        };

        const { asFragment }= render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        expect(screen.getByText('Test Item')).toBeInTheDocument();
        expect(screen.getByText('$100')).toBeInTheDocument();
        expect(screen.getByAltText('Test Item img')).toBeInTheDocument();

        const removeFromCartButton = screen.queryByTestId('removeFromCart-button');
        expect(removeFromCartButton).toBeInTheDocument();

        expect(asFragment()).toMatchSnapshot();
    });

    test('should render the item (Snapshot) when removeButton is false', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id',
            removeButton: false
        };

        const { asFragment }= render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        expect(screen.getByText('Test Item')).toBeInTheDocument();
        expect(screen.getByText('$100')).toBeInTheDocument();
        expect(screen.getByAltText('Test Item img')).toBeInTheDocument();

        const removeFromCartButton = screen.queryByTestId('removeFromCart-button');
        expect(removeFromCartButton).toBeNull();

        expect(asFragment()).toMatchSnapshot();
    });

    test('should show "Add to Cart" on the button when item is not in cart', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );
    
        expect(screen.getByText('Add to Cart')).toBeInTheDocument();
    });
    
    test('should show "Added in Cart" on the button when item is in cart', () => {
        const updatedState = {
            ...initialState,
            cartItems: {
                cartItems: [{
                    id: 1,
                    name: 'Test Item',
                    price: 100,
                    imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
                    item: 123,
                    color: 'red',
                    uniqueId: 'unique-id'
                }],
                loadingCart: false,
                errorCart: ''
            }
        };

        const store = mockStore(updatedState);

        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };
    
        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );
    
        expect(screen.getByText('Added in Cart')).toBeInTheDocument();
    });

    test('should dispatch postFavorite when the item is not in favorites and the star icon wrapper is clicked', () => { 
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        fireEvent.click(screen.getByTestId('star-icon-wrapper'));

        const actions = store.getActions();

        expect(actions[0].type).toBe('ADD_FAVORITE');
        expect(actions[1].type).toBe('UPDATE_ITEM_FAVORITE_STATUS');
    });

    test('should dispatch fetchDeleteFavorite when the item is in favorites and the star icon wrapper is clicked', () => { 
        const updatedState = {
            ...initialState,
            favorites: {
                favorites: [{
                    id: 1,
                    name: 'Test Item',
                    price: 100,
                    imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
                    item: 123,
                    color: 'red',
                    uniqueId: 'unique-id'
                }],
                loadingFavorites: false,
                errorFavorites: ''
            }
        };

        const store = mockStore(updatedState);

        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        fireEvent.click(screen.getByTestId('star-icon-wrapper'));

        const actions = store.getActions();

        expect(actions[0].type).toBe('DELETE_FAVORITE');
        expect(actions[1].type).toBe('UPDATE_ITEM_FAVORITE_STATUS');
    });

    test('should dispatch openModal action with addToCart type when "Add to Cart" button is clicked', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        const openModalMock = (modalType, onClickConfirm, onClickCancel, onClose) => ({ type: "OPEN_MODAL", payload: { modalType, onClickConfirm, onClickCancel, onClose }});

        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );
    
        fireEvent.click(screen.getByText('Add to Cart'));
    
        const actions = store.getActions();
        expect(actions[0]).toEqual(openModalMock('addToCart', expect.any(Function), expect.any(Function), expect.any(Function)));
    })

    test('should dispatch openModal action with addToCart type when "Added in Cart" button is clicked', () => {
        const updatedState = {
            ...initialState,
            cartItems: {
                cartItems: [{
                    id: 1,
                    name: 'Test Item',
                    price: 100,
                    imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
                    item: 123,
                    color: 'red',
                    uniqueId: 'unique-id'
                }],
                loadingCart: false,
                errorCart: ''
            }
        };

        const store = mockStore(updatedState);
        
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        const openModalMock = (modalType, onClickConfirm, onClickCancel, onClose) => ({ type: "OPEN_MODAL", payload: { modalType, onClickConfirm, onClickCancel, onClose }});

        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );
    
        fireEvent.click(screen.getByText('Added in Cart'));
    
        const actions = store.getActions();
        expect(actions[0]).toEqual(openModalMock('addToCart', expect.any(Function), expect.any(Function), expect.any(Function)));
    })

    test('should dispatch openModal action with removeFromCart type when remove from cart button is clicked', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id',
            removeButton: true // 
        };

        const openModalMock = (modalType, onClickConfirm, onClickCancel, onClose) => ({ type: "OPEN_MODAL", payload: { modalType, onClickConfirm, onClickCancel, onClose }});
    
        render(
            <Provider store={store}>
                <DisplayProvider>
                    <Item {...itemProps} />
                </DisplayProvider>
            </Provider>
        );

        fireEvent.click(screen.getByTestId("removeFromCart-button"));
    
        const actions = store.getActions();
        expect(actions[0]).toEqual(openModalMock('removeFromCart', expect.any(Function), expect.any(Function), expect.any(Function)));
    });

})
