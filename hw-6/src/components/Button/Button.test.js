import Button from "./Button";
import { render, screen, fireEvent } from '@testing-library/react';

describe('Test for Button Component', () => {
    test('should render the Button (Snapshot test)', () => {
        const onClickMock = jest.fn();
        const { asFragment } = render(<Button backgroundColor="red" text="Test Button" onClick={onClickMock} />)
        expect(asFragment()).toMatchSnapshot();
    })

    test("should call onClick by button click", () => {
        const onClickMock = jest.fn();
        render(<Button backgroundColor="red" text="Test Button" onClick={onClickMock} />);
        const btn = screen.getByRole('button', { name: "Test Button" });
        fireEvent.click(btn);
        expect(onClickMock).toHaveBeenCalled();
    }) 
})