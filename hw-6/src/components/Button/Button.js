import React from "react";
import PropTypes from 'prop-types';

function Button ({backgroundColor, text, onClick}) {

    return (
        <button
            style={{backgroundColor: backgroundColor}}
            onClick={onClick}
        >
        {text}
        </button>
    )
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    text: PropTypes.string.isRequired,
    onClick: PropTypes.oneOfType([PropTypes.func, PropTypes.object]).isRequired
};

Button.defaultProps = {
    backgroundColor: "transparent"
}

export default Button;