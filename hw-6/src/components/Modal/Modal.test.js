import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import * as redux from 'react-redux';
import * as cartActionCreators from '../../redux/cart/actionCreators';
import * as modalActionCreators from '../../redux/modal/actionCreators';
import Modal from './Modal';

jest.mock('react-redux', () => ({
    ...jest.requireActual('react-redux'),
    useDispatch: jest.fn(),
}));

const mockDispatch = jest.fn();

jest.mock('../../redux/cart/actionCreators', () => ({
    postCart: jest.fn(),
    fetchDeleteFromCart: jest.fn(),
}));
  
jest.mock('../../redux/modal/actionCreators', () => ({
    closeModal: jest.fn(),
}));

describe('Test for Modal Component', () => {

    beforeEach(() => {
        redux.useDispatch.mockReturnValue(mockDispatch);
        mockDispatch.mockClear();
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    test('should render the Modal component with props', () => {
        const actions = [
            { text: 'Action 1', onClick: jest.fn() },
            { text: 'Action 2', onClick: jest.fn() }
        ];
        render(
            <Modal header="Test Header" closeButton={true} text="Test content" actions={actions} onClose={jest.fn()} />
        );
    
        expect(screen.getByText('Test Header')).toBeInTheDocument();
        expect(screen.getByText('Test content')).toBeInTheDocument();
        expect(screen.getByText('Action 1')).toBeInTheDocument();
        expect(screen.getByText('Action 2')).toBeInTheDocument();
    });

    test('should display the close button when closeButton is true', () => {
        render(
            <Modal header="Test" closeButton={true} text="Content" actions={[]} onClose={jest.fn()} />
        );

        const closeButton = screen.getByTestId('close-button');
        expect(closeButton).toBeInTheDocument();
    });

    test('should not display the close button when closeButton is false', () => {
        render(
            <Modal header="Test" closeButton={false} text="Content" actions={[]} onClose={jest.fn()} />
        );

        const closeButton = screen.queryByTestId('close-button');
        expect(closeButton).toHaveStyle('display: none');
    });

    test('should call onClose when the modal container is clicked', () => {
        const onClose = jest.fn();
        const actions = [{ text: 'OK', onClick: jest.fn() }];

        render(
            <Modal header="Test Header" closeButton={true} text="Test content" actions={actions} onClose={onClose} />
        );

        fireEvent.click(screen.getByTestId('modal-container'));
        expect(onClose).toHaveBeenCalled();
    });

    test('should render action buttons based on the actions prop', () => {
        const actions = [
            { text: 'Action 1', onClick: jest.fn(), backgroundColor: 'red' },
            { text: 'Action 2', onClick: jest.fn(), backgroundColor: 'blue' }
        ];
        render(
            <Modal header="Test" closeButton={true} text="Content" actions={actions} onClose={jest.fn()} />
        );
    
        const actionButton1 = screen.getByText('Action 1');
        const actionButton2 = screen.getByText('Action 2');
    
        expect(actionButton1).toBeInTheDocument();
        expect(actionButton1).toHaveStyle('background-color: red');
    
        expect(actionButton2).toBeInTheDocument();
        expect(actionButton2).toHaveStyle('background-color: blue');
    });

    test('should call postCart and closeModal on "Confirm" button click for Modal with addToCart type', () => {
        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        const mockOnClickConfirm = jest.fn(() => {
            cartActionCreators.postCart(itemProps);
            modalActionCreators.closeModal();
        });

        render(
            <Modal 
                header="Add Product to Cart?" 
                actions={[
                    {
                        text: "Confirm", 
                        onClick: mockOnClickConfirm 
                    },
                    {
                        text: "Cancel",
                        onClick: jest.fn()
                    }
                ]} 
                onClose={jest.fn()} 
            />);
  
        fireEvent.click(screen.getByText('Confirm'));

        expect(cartActionCreators.postCart).toHaveBeenCalledWith(itemProps);
        expect(modalActionCreators.closeModal).toHaveBeenCalled();
    });

    test('should call closeModal on "Cancel" button click for Modal with addToCart type', () => {
        const mockOnClickCancel = jest.fn(() => {
            modalActionCreators.closeModal();
        });

        render(
            <Modal 
                header="Add Product to Cart?" 
                actions={[
                    {
                        text: "Confirm", 
                        onClick: jest.fn()
                    },
                    {
                        text: "Cancel",
                        onClick: mockOnClickCancel
                    }
                ]} 
                onClose={jest.fn()} 
            />);
  
        fireEvent.click(screen.getByText('Cancel'));
        expect(modalActionCreators.closeModal).toHaveBeenCalled();
    });

    test('should call removeFromCart and closeModal on "Confirm" button click for Modal with removeFromCart type', () => {

        const itemProps = {
            id: 1,
            name: 'Test Item',
            price: 100,
            imageUrl: 'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg',
            item: 123,
            color: 'red',
            uniqueId: 'unique-id'
        };

        const mockOnClickConfirm = jest.fn(() => {
            cartActionCreators.fetchDeleteFromCart(itemProps);
            modalActionCreators.closeModal();
        });
          

        render(
            <Modal 
                header="Do you want to delete this product?" 
                actions={[
                    {
                        text: "Confirm", 
                        onClick: mockOnClickConfirm 
                    },
                    {
                        text: "Cancel",
                        onClick: jest.fn()
                    }
                ]} 
                onClose={jest.fn()} />);
  
        fireEvent.click(screen.getByText('Confirm'));

        expect(cartActionCreators.fetchDeleteFromCart).toHaveBeenCalledWith(itemProps);
        expect(modalActionCreators.closeModal).toHaveBeenCalled();
    });

    test('should call closeModal on "Cancel" button click for Modal with removeFromCart type', () => {
        const mockOnClickCancel = jest.fn(() => {
            modalActionCreators.closeModal();
        });

        render(
            <Modal 
                header="Do you want to delete this product?" 
                actions={[
                    {
                        text: "Confirm", 
                        onClick: jest.fn()
                    },
                    {
                        text: "Cancel",
                        onClick: mockOnClickCancel
                    }
                ]} 
                onClose={jest.fn()} 
            />);
  
        fireEvent.click(screen.getByText('Cancel'));
        expect(modalActionCreators.closeModal).toHaveBeenCalled();
    });
});

