import React from "react";
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import closeIcon from '../../constants/close_icon';
import styles from './Modal.module.scss';

function Modal ({header, closeButton, text, actions, onClose}) {
    
    const closeModal = (e) => {
        if (onClose && e.target.classList.contains("modal__container")) {
            onClose();
        }
    }

    const buttons = actions.map((action, index) => (
        <Button
            key={index}
            text={action.text}
            backgroundColor={action.backgroundColor}
            onClick={action.onClick}
        />
    ));

    return (
        <div className={styles.modal__container} data-testid="modal-container" onClick={closeModal}>
            <div className={styles.modal} data-testid="modal">
                <div className={styles.modal__header__container}>
                    <h2>{header}</h2>
                    <button onClick={onClose} data-testid="close-button" style={{ display: closeButton ? 'block' : 'none' }}>{closeIcon}</button>
                </div>
                <div className={styles.modal__content}>
                    <p>{text}</p>
                    <div className={styles.modal__content__buttons}>{buttons}</div>
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string.isRequired,
    closeButton: PropTypes.bool,
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    actions: PropTypes.arrayOf(PropTypes.object).isRequired,
    onClose: PropTypes.func.isRequired
};

Modal.defaultProps = {
    closeButton: true,
    text: ""
}

export default Modal;