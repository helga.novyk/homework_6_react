import React from 'react';
import { useField } from 'formik';
import { PropTypes } from 'prop-types';
import styles from './Input.module.scss';
import { PatternFormat } from 'react-number-format';

function Input({type, ...props}) {

    const [field, meta, helpers] = useField(props.name);
    const { setValue } = helpers;
    const { error, touched } = meta;

    if (props.name === 'cellPhone') {
        return (
            <div className={styles.root}>
                <label htmlFor={props.id}>{props.label || field.name}</label>
                <PatternFormat
                    {...field}
                    {...props}
                    format="(###) #### ###"
                    mask="_"
                    onValueChange={(values) => {
                        const { formattedValue } = values;
                        setValue(formattedValue);
                    }}
                />
                {touched && error && <p className={styles.error}>{error}</p>}
            </div>
        );
    }


    return (
        <div className={styles.root}>
            <label htmlFor={props.id}>{props.label || field.name}</label>
            <input {...field} {...props} />
            {touched && error && <p className={styles.error}>{ error }</p>}
        </div>
    )
}

Input.propTypes = {
    type: PropTypes.string,
}

Input.defaultProps = {
    type: "text",
}

export default Input;