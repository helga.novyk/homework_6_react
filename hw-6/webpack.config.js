const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
    entry: './src/index.js', // Точка входу вашого додатку
    output: {
        filename: 'bundle.js',
        path: path.resolve('C:\Users\Lenovo\Desktop\Web Projects\#React\homework_6_react\hw-6', 'dist'), // Директорія для зібраних файлів
    },
    module: {
        rules: [
            {
                test: /\.scss$/, // Використовується для обробки файлів .scss
                use: [
                    MiniCssExtractPlugin.loader, // Витягує CSS у окремі файли
                    'css-loader', // Перетворює CSS у CommonJS
                    'sass-loader' // Компілює SCSS у CSS
                ],
            },
            {
                test: /\.html$/, // Використовується для обробки файлів .html
                use: [
                    {
                        loader: 'html-loader', // Експортує HTML як рядок
                        options: { minimize: true },
                    },
                ],
            },
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                }
            }            
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './public/index.html', // Шаблон HTML файлу
            filename: './index.html', // Назва вихідного HTML файлу
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        }),
    ],
};
